$(document).ready(function () {

    $("#openSendEmail").on("click",function(){
        $("#openEmailModal").click()
    })


    // 邮件接口
    $("#sendBtn").on("click", function () {
        var tt = $
        if ($("#title").val() === "" || $("#content").val() === "") {
            return;
        }

        $.ajax({
            url: 'http://8.134.192.166:7004/sendEmail',
            // url: 'http://127.0.0.1:8080/sendEmail',
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify({
                title: $("#title").val(),
                content: $("#content").val()
            }),
            success: function (d) {
                tt("#closeEmailModal").click()
                // 置空
                tt("#title").val() = ""
                tt("#content").val() = ""

                // 发送成功 CSS
                $("#msg").css({
                    "display": "block",
                    "position": "absolute",
                    "top": "30%",
                    "left": "33%"
                })
                // 两秒后消失
                setTimeout(function () {
                    $("#msg").css({
                        "display": "none"
                    })
                }, 2000);

            }
        })
    });
});