# vue3学习笔记

#### 介绍
* 自用存储练习vue3代码托管仓库，交流请滴滴联系方式1943133326@qq.com
* 前端技术栈 ： vue3 vite elementUI-plus typescript axios vue-router vuex 
```sh
  初始化项目后提交代码
    git init
    git remote add origin https://gitee.com/huishao-online-didi/vue3-learning-notes.git
    git add .
    git commit -m "初始化项目"
    git push origin master
  更新代码
    git status
    git add .
    git commit -m "修改readme"
    git push origin master
    git status

  远端拉取代码
    git merge origin/master
    
    git pull --rebase origin master

```
