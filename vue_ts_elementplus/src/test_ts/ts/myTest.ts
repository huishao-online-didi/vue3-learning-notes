export {}
let myAdd: (baseValue: number, increment: number) => number =
    function(x, y) { return x + y; };


let myMap1 : () => Map<string,string> = 
    function(){
        let m = new Map();
        m.set("os", "ubuntu");
        return m;
    };

console.log("result = > ",myAdd(1,2))
console.log("result2 = > ",myMap1().get("os"))



interface Person { 
    age:number 
} 
  
interface Musician extends Person { 
    instrument:string 
} 
 var drummer = <Musician>{}; 
 drummer.age = 27 
//  drummer.instrument = "Drums" 
 console.log("年龄:  "+drummer.age)
//  console.log("喜欢的乐器:  "+drummer.instrument)



interface RunOptions { 
    program:string; 
    commandline:string[]|string|(()=>string); 
} 
var options1:RunOptions = {program:"test1",commandline:["Hello","World"]}; 
// console.log(options1.commandline[0])



interface ILoan { 
    interest:number 

    cooo:(()=>Map<string,string>)

} 
  
 class AgriLoan1 implements ILoan { 
    interest:number 
    rebate:number 

    cooo=function(){
        return new Map<string,string>;
    }
    
    constructor(interest:number,rebate:number) { 
       this.interest = interest 
       this.rebate = rebate 
    } 
 } 
  
 var obj = new AgriLoan1(10,1) 
 console.log("利润为 : "+obj.interest+"，抽成为 : "+obj.rebate )