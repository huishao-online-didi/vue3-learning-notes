import { createRouter , createWebHashHistory,RouteRecordRaw} from 'vue-router';
import { App } from 'vue'
// es6 路由配置
const routes:RouteRecordRaw[] = [
    {
        path:'/my',
        name:'my',
        component: () => import('../views/my.vue')
    },
    {
        path:'/login',
        name:'login',
        component: () => import('../views/loginForm.vue')
    },
    {
        path:'/studentList',
        name:'studentList',
        component: () => import('../views/studentList.vue')
    },
    {
        path:'/form',
        name:'form',
        component: () => import('../views/form.vue')
    },
    {
        path:'/studentCrud',
        name:'studentCrud',
        component: () => import('../views/studentCrud.vue')
    },
    {
        path:'/studentList',
        name:'studentList',
        component: () => import('../views/studentList.vue')
    }
]
const router = createRouter ({
    history: createWebHashHistory(),
    routes // 路由配置


});


export const initRouter = (app:App<Element>) => {
    app.use(router)
}

// export default router