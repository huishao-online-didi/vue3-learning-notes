import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import { initRouter } from './router'

const app = createApp(App)
// 初始化路由 
initRouter(app)
app.use(ElementPlus)
app.mount('#app')
