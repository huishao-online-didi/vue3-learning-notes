import request from '../../request'
import { Student } from '../../../interface/Student'


// 搜索方法
export function QueryStudent(data: any,page: number, limit: number) {
    return request({
        url: '/api/student/Query/'+page + '/' + limit,
        method: 'post',
        data:data
    })
}

// 查询crud列表
export function listStudent(page: number, limit: number) {
    return request({
        url: '/api/student/' + page + '/' + limit,
        method: 'get',
    })
}

// 查询crud详细
export function getStudent(id: string) {
    return request({
        url: '/api/student/' + id,
        method: 'get'
    })
}

// 新增crud
export function addStudent(data: any) {
    return request({
        url: '/api/student',
        method: 'post',
        data: data
    })
}

// 修改crud
export function updateStudent(data: any) {
    return request({
        url: '/api/student',
        method: 'put',
        data: data
    })
}

// 删除crud
export function delStudent(id: string) {
    return request({
        url: '/api/student/' + id,
        method: 'delete'
    })
}

// 批量删除crud
export function delListStudent(ids: string) {
    return request({
        url: '/api/student?idList=' + ids,
        method: 'delete'
    })
}

