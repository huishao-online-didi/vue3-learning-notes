import axios from 'axios';

const baseRequert = axios.create({
    baseURL:"http://192.168.2.248:9998/",
    timeout:300000
})


//拦截

baseRequert.interceptors.request.use(
    config => {
        return config;
    }, err => {
        return Promise.reject(err)
    }
)

baseRequert.interceptors.response.use(
    result => {
        return result;
    },
    err =>{
        return Promise.reject(err);
    }
)


export default baseRequert;