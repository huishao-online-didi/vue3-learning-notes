export interface Student{
    id: string,
    name: string,
    age: number,
    grade: string,
    teacherName: string,
    status: string,
    session: string,
    type: string
}